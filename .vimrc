" UI/UX
syntax on
set nocompatible    " Dont try to be legacy vi compatible
set encoding=utf-8  " UTF-8 always
set backspace=2     " Fix backspace key
set laststatus=2    " Show status at bottom with filename
set noeol           " Prevent vim from add/rm newline
set number          " Show line numbers
set ruler           " Show current line/column number
set showcmd         " Show command in bottom bar
set cursorline      " Highlight current line
set lazyredraw      " Redraw only when we need to
set incsearch       " Search as characters are entered
set autoindent      " Vim indents from cur line

"Spaces & Tabs
set tabstop=4       " Number of visual spaces per TAB
set softtabstop=4   " Number of spaces in tab when editing
set shiftwidth=4
