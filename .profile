# This file is read each time a login shell is started.
# All other interactive shells will only read .bashrc.

export EDITOR=/usr/bin/vim
export NO_AT_BRIDGE=1
export _JAVA_AWT_WM_NONREPARENTING=1
export MAN_POSIXLY_CORRECT=1

export CC=clang
export CC_LD=lld
export PATH="$HOME/.cargo/bin:$PATH"

# launch sway
if [ $(tty) = "/dev/tty1" ]; then
	exec sway
fi
