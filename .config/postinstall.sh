#!/usr/bin/env sh

# Check if script run with sudo
if [ "$USER" = root ]; then
	echo 'Cant run as root'
	exit
fi

# Set variables
RED='\033[1;31m'
CLEAR='\033[0m'
unwanted="
	bbswitch
	gnome-keyring gnome-online-accounts gnome-themes-accessibility gvfs
	icewm
	lightdm
	ModemManager
"
wanted="
	bluez brightnessctl
	chrony clang
	firefox
	foot
	gcc
	htop
	i3blocks iwd
	lld
	make mako Mesa meson
	nasm neofetch
	opi
	pipewire pipewire-alsa
	rtkit
	ShellCheck sway
	tlp
	wofi
"

# Initial questions
printf "Want to install codecs? (y/n) "
read -r inscode

printf "Want to setup bluetooth headset? (y/n) "
read -r setupbt

# Refresh and update
sudo zypper -n dup

# Add myself to video group
sudo usermod -aG video "$USER"

# Remove and lock unwanted
echo "$unwanted" | xargs sudo zypper -n rm -Cu
echo "$unwanted" | xargs sudo zypper al

# Package install
echo "$wanted" | xargs sudo zypper -n install

# Install codecs
if [ "$inscode" = y ]; then
	yes | opi codecs
fi

# Systemd services
sudo systemctl disable postfix
sudo systemctl disable auditd
sudo systemctl disable purge-kernels
sudo systemctl disable NetworkManager-dispatcher
sudo systemctl disable NetworkManager-wait-online
sudo systemctl disable lvm2-monitor.service
sudo systemctl disable lvm2-lvmpolld.socket
sudo systemctl mask postfix
sudo systemctl mask auditd
sudo systemctl enable tlp
sudo systemctl enable chronyd
sudo systemctl enable --now bluetooth
# Pipewire needs these to be run w/o sudo
systemctl enable  --user pipewire

# Create git projects dir
mkdir ~/projects || true

# Change from wpa to iwd
if ! grep -q iwd /etc/NetworkManager/NetworkManager.conf; then
	printf "[device]\nwifi.backend=iwd\n" | sudo tee -a /etc/NetworkManager/NetworkManager.conf > /dev/null
	sudo systemctl stop NetworkManager
	sudo systemctl disable --now wpa_supplicant
	sudo systemctl restart NetworkManager
fi

# Pair and trust bluetooth headset
if [ "$setupbt" = y ]; then
	# Get MAC of bluetooth headset and make useable from other scripts
	bluetoothctl power on
	bths=$(bluetoothctl --timeout 10 scan on | grep "Epic Air" | cut -d ' ' -f3)
	if [ -n "$bths" ]; then
		bluetoothctl pair "$bths"
		bluetoothctl trust "$bths"
		grep -q "export headset" ~/.bashrc || echo "export headset=$bths" >> ~/.bashrc
	else
		printf "%sBLUETOOTH HEADSET NOT FOUND!%s\n" "$RED" "$CLEAR"
	fi
fi

echo "Finished!"
